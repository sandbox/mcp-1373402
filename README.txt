Description
===========

This module enables you to un-/hide nodes and comments.

The concept of 'hiding' has to be understood as something in between
 - being published and shown in all sorts of places and
 - being unpublished
it shall be visible if you go to another URI, e.g. hidden/, but
not when viewing nodes and comments in the normal way or by using
the search function.

The purpose is to help transparency of the 'open editing' process of an
'open publishing' site.

Features
========

 - Articles unpublished as hidden are available to users with 'access hidden'
     privileges.
 - Hidden articles are always displayed with the reason that they are hidden,
     and custom css class.
 - The reason for hiding can be selected from a list or/and typed in.
 - The list of reasons for hiding can be set from admin.

Removed Features!
=================

The d6 Version of hidden had a filter, report for hiding and an email report 
feature. These features have been removed.
Instead of the filter you can use the simple_regex_filter project
I'm sure there are some project for reporting comments/nodes and the email
report is also somehow possible. If you know how, please let me know.
Maybe i've to implement an hidden action or so...

Installation
============
After enabling the module, set access permissions at admin/user/access:
 - 'access hidden' permission to view hidden nodes and comments.
 - 'mark as hidden' permission to un-/mark nodes and comments as hidden
 - 'administer hidden' permission allows changes to filters and settings
      like what reports are made.

You probably want the role that can 'mark as hidden' to be able to 'access
hidden' as well.

Visit /admin/settings/hidden to set some settings of the module and create
predefined reasons.

You can create a view of hidden nodes/comments. Please be aware that this
version doesn't automatically block access to the hidden view to robots.
You will have to do this by hand, for example by installing a robots.txt 
file in the websites top directory containing the lines:
  User-agent: *
  Disallow: /path/to/hidden/view

Usage
======
Every node or comment has an extra link. 

Users with 'mark as hidden' permission will have a link to hide or unhide. The
following page allows them to explain why it should be hidden using one of the
default reasons set (see installation) and/or using free text notes.

The forms have a public text box. This text, which can contain some HTML (as
Dupal 'Basic HTML' settings), will be displayed with the hidden post. 
There is also a private text box for users with 'mark as hidden' permission.
This will only be displayed to other users with 'mark as hidden' permission.

Mass Un-/Hiding for nodes:
 - Go to admin/content/node.
 - Tick the nodes you want to mass un-/hide.
 - Select Un-/Hide in the dropdown list of operations and click Update.
    The reason 'Policy Violation' will be applied automatically.

Mass Unhiding for nodes and comments:
 - Go to admin/content/node/list/hidden or admin/content/comment/list/hidden
 - Tick the nodes you want to mass unhide.
 - Click update

Notes
=====
 - The private notes section of the report, hide and filter forms, for users 
    with 'mark as hidden' privileges, can be used to leave messages about the
    content that will not be displayed.
 - You don't need to make users full admin users to be able to deal with
    hidden posts.
 - Users who can administer nodes and comments will see hidden content with
    notes on pages other than hidden views.

Credits
=======
Sebastian Henschel <aotearoa <at> kodeaffe <dot> de>
  wrote original implementation of unpublishing hidden method (v1)
  for Aotearoa Indymedia Centre
ekes <ekes <at> aktivix <dot> org>
  rewritten and features implemented (v2)
  updated for Drupal 6 and testing framework
Testing: kameron, alster 

$Id: README.txt,v 1.3 2008/12/18 15:29:06 ekes Exp $
