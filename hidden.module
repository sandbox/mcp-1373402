<?php
// $Id: hidden.module,v 1.8.2.1 2009/03/13 11:44:09 ekes Exp $

/**
 * @file
 * Module to create, track and display hidden content.
 */


/********************************************************************
 * Standard hooks
 */

 /**
 * Implementation of hook_boot().
 */
function hidden_boot() {
  if (module_exists('views')) {
    include_once drupal_get_path('module', 'hidden') .'/views/hidden.views.inc';
  }
}

/**
 * Implementation of hook_init().
 */
function hidden_init() {
  drupal_add_css(drupal_get_path('module', 'hidden') .'/hidden.css', 'module', 'all', FALSE);
}

/**
 * Implementation of hook_help().
 */
function hidden_help($path, $arg) {
  $output = '';

  switch ($path) {
    case 'admin/settings/hidden/reasons':
      $output = '<p>'. t('The default reasons offered for hiding an item. Disabled reasons do not show in the list.') .'</p>';
      break;
    case 'admin/settings/hidden/reasons/edit':
      $output = '<p>'. t('Changing the text of a reason will change it for all previously hidden items as well.') .'</p>';
      break;
    case 'admin/settings/hidden':
      $output = '<p>'. t('Settings for the hidden module. You can create/edit predefined reasons and enfoce public messages.') .'</p>';
      break;
    case 'admin/help#hidden':
      $output = '<p>'. t('Hidden enables you to un-/hide nodes and comments.') ."</p>\n".
        '<p>'. t("Every node or comment has a link called 'hide' (or 'unhide' if hidden).") ."</p>\n".
        '<p>'. t("To 'hide':") ."<br />\n".
        '- '. t('Click the link.') ."<br />\n".
        '- '. t('On the following page you have to supply a reason why the item will be hidden before you submit the form.') ."</p>\n".
        '<p>'. t('Hidden items should not appear in any listings for non-admin users except under hidden/.') ."</p>\n".
        '<p>'. t("To 'unhide':") ."<br />\n".
        '- '. t("Click the link after the contents' title/subject or click on the contents' title/subject.") ."<br />\n".
        '- '. t("Then click 'unhide'.") ."</p>\n";
      break;
    }

    return $output;
}

/**
 * Implementation of hook_perm().
 */
function hidden_permission() {
  return array(
  'mark as hidden' => array(
    'title' => t('Mark as hidden'),
    'description' => t('Mark content as hidden')
    ), 
  'administer hidden' => array(
    'title' => t('Adminster hidden'),
    'description' => t('Adminster hidden content')
    ),
  );
}

/**
 * Implementation of hook_theme().
 */
function hidden_theme() {
  return array(
    'hidden_view_reason' => array(
      'variables' => array(
        'hidden' => NULL,
        'content' => '',
        'full' => TRUE,
        ),
      )
  );
}

/**
 * Implementation of hook_menu().
 */
function hidden_menu() {
  $items = array();

  $items['admin/settings/hidden'] = array(
    'title' => 'Hidden settings',
    'description' => 'Administer settings for the hidden module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hidden_admin_settings'),
    'access arguments' => array('administer hidden'),
  );
  $items['admin/settings/hidden/reasons'] = array(
      'title' => 'Hidden reasons.',
      'description' => 'Manage reasons for hiding.',
      'page callback' => 'hidden_admin_settings_reasons',
      'access arguments' =>  array('administer hidden'),
      'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/hidden/reasons/add'] = array(
      'title' => 'Add hidden reason',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('hidden_admin_reason_form', 'add'),
      'access arguments' => array('administer hidden'),
      'type' => MENU_CALLBACK,
  );
  $items['admin/settings/hidden/reasons/edit'] = array(
      'title' => 'Edit hidden reason',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('hidden_admin_reason_form', 'edit'),
      'access arguments' => array('administer hidden'),
      'type' => MENU_CALLBACK,
  );
  $items['admin/settings/hidden/reasons/delete'] = array(
      'title' => 'Delete reason',
      'page callback' => 'hidden_admin_reason_delete',
      'access arguments' => array('administer hidden'),
      'type' => MENU_CALLBACK,
  );
  $items['admin/settings/hidden/reasons/enable'] = array(
      'title' => 'Enable hidden reason',
      'page callback' => 'hidden_admin_reason_toggle',
      'page arguments' => array('enable'),
      'access arguments' => array('administer hidden'),
      'type' => MENU_CALLBACK,
  );
  $items['admin/settings/hidden/reasons/disable'] = array(
      'title' => 'Disable hidden reason',
      'page callback' => 'hidden_admin_reason_toggle',
      'page arguments' => array('disable'),
      'access arguments' => array('administer hidden'),
      'type' => MENU_CALLBACK,
  );

  $items['hidden/%/%/hide'] = array(
    'title' => 'Hide post',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('hidden_hide', 1, 2),
    'access arguments' => array('mark as hidden'),
    'type' => MENU_CALLBACK,
  );
  $items['hidden/%/%/unhide'] = array(
    'title' => 'Don\'t hide',
    'page callback' => 'hidden_unhide',
    'page arguments' => array(1, 2),
    'access arguments' => array('mark as hidden'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
* Menu callback; Hidden admin change reasons for hiding
*/
function hidden_admin_settings_reasons() {
  $enabled = array();
  $disabled = array();
  $reasons = hidden_reason_get_all(FALSE);
  foreach ($reasons as $reason) {
    if ($reason->enabled) {
      $enabled[] = array(
        check_plain($reason->title),
        l(t('edit'), 'admin/settings/hidden/reasons/edit/'. $reason->rid),
        l(t('disable'), 'admin/settings/hidden/reasons/disable/'. $reason->rid),
        l(t('delete'), 'admin/settings/hidden/reasons/delete/' . $reason->rid),
      );
    }
    else {
      $disabled[] = array(
        check_plain($reason->title),
        l(t('edit'), 'admin/settings/hidden/reasons/edit/'. $reason->rid),
        l(t('enable'), 'admin/settings/hidden/reasons/enable/'. $reason->rid),
        l(t('delete'), 'admin/settings/hidden/reasons/delete/'. $reason->rid),
      );
    }
  }
  if (count($enabled) == 0) {
    $enabled[] = array(t('No enabled reasons'), '', '');
  }
  if (count($disabled) == 0) {
    $disabled[] = array(t('No disabled reasons'), '', '');
  }

  $page = theme(
  	'links', array(
  		'links' => array(
  			'newreason' => array('title' => t('Add new reason'), 'href' => 'admin/settings/hidden/reasons/add/')
         ),
  		'attributes' => array()
    )
  );
  $header = array(t('Reason'), array('data' => t('Operations'), 'colspan' => '2'));
  $page .= theme('table', array('header'=>$header, 'rows'=>$enabled));
  $page .= theme('table', array('header'=>$header, 'rows'=>$disabled));

  return $page;
}

/**
* Menu callback; Module Configuration.
*
* @ingroup forms
*/
function hidden_admin_settings($form_state) {
  $form['hidden_public_notes'] = array(
    '#title' => t('Allow public notes'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hidden_public_notes', TRUE),
    '#description' => t('If unchecked, the form for public notes about hidden content will be disabled, only predefined reasons are available.')
  );
  $form['hidden_private_notes'] = array(
    '#title' => t('Allow private notes'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hidden_private_notes', TRUE),
    '#description' => t('Allow private notes.')
  );
  $form['hidden_public_notes_required'] = array(
    '#title' => t('Require public notes'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hidden_public_notes_required', FALSE),
    '#description' => t('If checkt, public notes must be given.')
  );
  return system_settings_form($form);
}

/**
* Menu callback; Admin new or edit hidden reason
*
* @ingroup forms
* @see hidden_admin_reasons_form_submit
*/
function hidden_admin_reason_form($form, &$form_state, $op, $rid=0) {
  $rid = (int)$rid;
  if ($op == 'edit') {
    if (!($item = db_query('SELECT * FROM {hidden_reasons} WHERE rid = :rid', array(':rid'=>$rid))->fetchAssoc())) {
      drupal_not_found();
      return;
    }
  }
  elseif ($op == 'add') {
    $item=array('rid' => '0', 'enabled' => '1', 'title' => "", 'description' => "");
  }
  else {
    watchdog("hidden", "Invalid op %op in hidden_admin_reasons_form", array('%op'=>$op), WATCHDOG_ERROR);
    drupal_goto('admin/settings/hidden/reasons');
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => check_plain($item['title']),
    '#maxlength' => 255,
    '#description' => t('The name of the reason for hiding.'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $item['description'], // I want to filter this
    '#description' => t('The description of the reason. This could include a link to editorial guidelines.'),
  );
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $item['enabled'],
    '#description' => t('If this reason is available to be chosen from the hiding menu.'),
  );
  $form['rid'] = array('#type' => 'value', '#value' => $item['rid']);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

function hidden_admin_reason_delete($rid) {
  if (!($item = db_query('SELECT * FROM {hidden_reasons} WHERE rid = :rid', array(':rid'=>$rid))->fetchAssoc())) {
    drupal_not_found();
    return;
  } else {
    $countnode = db_update('hidden_node')->fields(array('rid'=>0))->condition('rid', $rid, '=')->execute();
    $countcom = db_update('hidden_comment')->fields(array('rid'=>0))->condition('rid', $rid, '=')->execute();
    $rem = db_delete('hidden_reasons')->condition('rid', $rid)->execute();
    drupal_set_message(t('Changed reason from :countnode nodes and :countcom coments from :rtitle to default reason.', array(':countnode'=>$countnode, ':countcom'=>$countcom, ':rtitle'=>$item['title'])));
    drupal_goto('admin/settings/hidden/reasons');
  }
}

/**
* Implementation of form API hook; submit hidden_admin_reasons_form().
*/
function hidden_admin_reason_form_submit($form, &$form_state) {
  $title = (string) $form_state['values']['title'];
  $description = (string) $form_state['values']['description'];
  $rid = (int) $form_state['values']['rid'];
  $enabled = (bool) $form_state['values']['enabled'];

  if ($rid) {
    $result = hidden_reason_update($title, $description, $enabled, $rid);
  }
  else {
    $result = hidden_reason_create($title, $description, $enabled);
  }

  if ($result == FALSE) {
    drupal_set_message(t('Error saving reason %title', array('%title' => $title)), 'error');
    watchdog('hidden', t('Error saving reason %title'), array('%title' => $title), WATCHDOG_ERROR);
  }
  elseif ($result == $rid) {
    drupal_set_message(t('The reason %title has been updated.', array('%title' => $title)));
    watchdog('hidden', t('The reason %title has been updated'), array('%title' => $title), WATCHDOG_INFO);
  }
  else {
    drupal_set_message(t('The reason %title has been added.', array('%title' => $title)));
    watchdog('hidden', t('The reason %title has been added'), array('%title' => $title), WATCHDOG_INFO);
  }

  $form_state['redirect'] = 'admin/settings/hidden/reasons';
}

/**
* Menu callback; form to hide a node or comment.
*
* @param $form_state
* @param $type
*   string 'node' or 'comment'
* @param $id
*   int nid or cid
* @return
*   form.
* @ingroup forms
* @see hidden_hide_validate()
* @see hidden_hide_submit()
*/
function hidden_hide($form, &$form_state, $type, $id) {
  $type = (string) $type;
  $id = (int) $id;

  //$hidden = (object) _hidden_hide_form_hidden_get($type, $id);
  $hidden = (object) hidden_hidden_get($type, $id);
  /*if (! $item = hidden_hide_form_item_get($type, $id)) {
    drupal_goto();
  }*/
  $item = hidden_hide_form_item_get($type, $id);

  drupal_set_title(t('Hide @title', array('@title' => $item->title)), PASS_THROUGH);
  $form = hidden_hide_form($hidden);
  $form['#content_type'] = $type;
  $form['#content_id'] = $id;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Hide'),
  );

  return $form;
}

/**
* Retrieves node or comment.
*
* @todo this is generic MOVE
*
* Moves comment->subject to title to make consistent handling of both nodes and comments.
* Calling functions are only using the title, but for abstraction using _load commands -
* eg. should also work with node comments then.
*
* @param string $type 'node' or 'comment'.
* @param int $id nid or cid
* @return object node or comment.
*/
function hidden_hide_form_item_get($type, $id) {
  if ($type == 'comment') {
    if (! $item = comment_load($id)) {
      // @todo error
      return FALSE;
    }
    $item->title = $item->subject;
  }
  else {
    if (! $item = node_load($id)) {
      // @todo error
      return FALSE;
    }
    $item->cid = 0;
  }

  $item->type = $type;
  $item->id = $id;

  return $item;
}

/**
* Form fields for hide form.
*
*/
function hidden_hide_form($hidden = stdClass) {
  global $user;
  $form = array();

  if (user_access('administer hidden')) {
    $form['user'] = array(
    	'#type' => 'textfield',
      	'#title' => t('Hidden by'),
      	'#maxlength' => 60,
      	'#autocomplete_path' => 'user/autocomplete',
      	'#default_value' => isset($hidden->name) ? check_plain($hidden->name) : check_plain($user->name),
    );
  }
  else {
    $form['user'] = array(
		'#type' => 'textfield',
		'#title' => t('Hidden by'),
		'#value' => check_plain($user->name),
		'#disabled' => TRUE,
    );
  }
  //$reasons = hidden_reasons_get_options();
  $reasons = hidden_reason_get_all();
  $options = array(0 => t('No standard reason'));
  foreach ($reasons as $rid => $reason ) {
    $options[$rid] = $reason->title;
  }
  
  $form['reason'] = array(
		'#type' => 'radios',
		'#title' => t('Preset Reason'),
		'#default_value' => isset($hidden->rid) ? $hidden->rid : 0,
    	'#options' => $options,
    	'#description' => t('Preset reason why this item shall be hidden.'),
  );

  if (variable_get('hidden_public_notes', TRUE)) {
    $form['publicnote'] = array(
  		'#type' => 'textarea',
    	'#title' => t('Your Public notes'),
    	'#default_value' => isset($hidden->publicnote) ? $hidden->publicnote : '',
		'#rows' => 5,
    	'#maxlength' => 1028,
		'#description' => t("Type your reason why this item shall be hidden."),
		'#required' => variable_get('hidden_public_notes_required', FALSE),
    );
  }

  $form['privatenote'] = array(
    '#type' => 'textarea',
    '#title' => t('Your Private notes'),
    '#default_value' => isset($hidden->privatenote) ? $hidden->privatenote : '',
    '#rows' => 5,
    '#maxlength' => 1028,
	'#description' => t("Notes that can only be seen by other users who can (un)hide content."),
  );
  return $form;
}

/**
* Implementation of form API hook; hide content submit hidden_hide().
*/
function hidden_hide_validate($form, &$form_state) {
  if (user_access('administer hidden')) {
    // Validate the "hidden by" field.
    if (!empty($form_state['values']['user']) && !($user = user_load_by_name($form_state['values']['user']))) {
      form_set_error('user', t('The username %name does not exist.', array('%name' => $form_state['values']['user'])));
    }
  }
}

/**
 * Implementation of form API hook; hide content submit hidden_hide().
 */
function hidden_hide_submit($form, &$form_state) {
  if (! user_access('mark as hidden')) {
    drupal_access_denied();
  }

  $type = (string) $form['#content_type'];
  $id = (int) $form['#content_id'];

  if (user_access('administer hidden')) {
    $user = user_load_by_name($form_state['values']['user']);
    $uid = $user->uid;
  }
  else {
    global $user;
    $uid = $user->uid;
  }

  $rid = (int) $form_state['values']['reason'];
  $rid = hidden_reason_check($rid) ? $rid : 0;
  $public = (string) $form_state['values']['publicnote'];
  $private = (string) $form_state['values']['privatenote'];

  if (hidden_hidden_hide($type, $id, $uid, $rid, $public, $private)) {
    drupal_set_message(t('Hidden %type.',array('%type' => $type)));
  }
  else {
    drupal_set_message(t('Error hiding %type.', array('%type' => $type)), 'error');
  }
  /**
   * The redirect challenge.
   *
   * What do users want/expect to see next?
   * What are they allowed to see?
   */
  if (! user_access('administer content')) {
    if (isset($_REQUEST['destination'])) {
      unset($_REQUEST['destination']);
    }
  }
  $form_state['redirect'] = 'hidden/'. $type .'/'. $id;
}

/**
* Menu callback; unhide a node or comment.
*
* @param $hidden
*   hidden details object see hidden_hidden_check()
*/
function hidden_unhide($type, $id) {
  if (hidden_hidden_unhide($type, $id)) {
    drupal_set_message(t('Hidden %type unhidden', array('%type' => $type)));
    if ($type == 'node') {
      drupal_goto('node/'. $id);
    }
    else {
      $nid = comment_load($id)->nid;
      drupal_goto('node/'. $nid, array('fragment'=>'comment-'. $id));
    }
  }
  else {
    drupal_set_message(t('Error unhiding %type.', array('%type' => $type), 'error'));
    drupal_goto();
  }

}

/**
 * Menu callback; hidden_reasons_admin en/dis-able a reason
 */
function hidden_admin_reason_toggle($action, $rid) {
  $rid = (int)$rid;

  if ($action == 'enable') {
    $result = hidden_reason_enable($rid);
  }
  elseif ($action == 'disable') {
    $result = hidden_reason_disable($rid);
  }
  else {
    watchdog('hidden', t('hidden_reasons_admin_able() called with invalid action'), array(), WATCHDOG_ERROR);
  }

  if ($result) {
    drupal_set_message(t('Reason %action', array('%action' => $action ? 'enabled' : 'disabled')));
  } 
  else {
    drupal_set_message(t('Error %action reason', array('%action' => $action ? 'enabling' : 'disabling')));
  }

  drupal_goto('admin/settings/hidden/reasons');
}


/********
 *  hooks
 */
/**
* Implementation of hook_node_operations().
*/
function hidden_node_operations() {
  $operations = array(
    'hide' => array(
      'label' => t('Hide'),
      'page callback' => 'hidden_operations_hide',
  ),
    'unhide' => array(
      'label' => t('Unhide'),
      'page callback' => 'hidden_operations_unhide',
  ),
  );

  return $operations;
}


/**
 * Implementation of hook_node_view().
 * adds reasons and div tags to viewed nodes.
 * @TODO: make cleaner, with class
 */
function hidden_node_view($node, $view_mode, $langcode) {
      if (!$node->status) {
        if ($hidden=hidden_hidden_get('node', $node->nid)) {
          if ($view_mode == 'teaser') {
            $node->content['body'][0]['#markup'] = theme('hidden_view_reason', array('hidden'=>$hidden, 'content'=>$node->content['body'][0]['#markup'], 'full'=>FALSE));
          } else {
            $node->content['body'][0]['#markup'] = theme('hidden_view_reason', array('hidden'=>$hidden, 'content'=>$node->content['body'][0]['#markup'], 'full'=>TRUE));
          }
          $node->content['title'] = t('HIDDEN: @title', array('@title' => $node->title));
        }
      }
  $node->content['links']['hidden'] = array('#links' => hidden_links('node', $node));
}

/**
 * Implementation of hook_comment_view
 * Enter description here ...
 * @param unknown_type $comment
 * @TODO: make cleaner, with class
 */
function hidden_comment_view($comment) {
      if (!$comment->status) {
        if ($hidden=hidden_hidden_get('comment', $comment->cid)) {
          //var_dump($comment);
          $comment->content['comment_body'][0]['#markup'] = theme('hidden_view_reason', array('hidden'=>$hidden, 'content'=>$comment->content['comment_body'][0]['#markup'], 'full'=>TRUE));
          $comment->subject = t('HIDDEN: @title', array('@title' => $comment->subject));
        }
      }
  $comment->content['links']['hidden'] = array('#links' => hidden_links('comment', $comment));
}

/**
 * Implementation of hook_node_delete
 * Enter description here ...
 * @param unknown_type $node
 */

function hidden_node_delete($node) {
  if (hidden_hidden_delete('both', $node->nid)) {
    watchdog('hidden', t('Deleted hidden node and/or attached comments.'), array(), WATCHDOG_INFO);
  }
  else {
    watchdog('hidden', t('Error deleting hidden node and/or comments.'), array(), WATCHDOG_ERROR);
  }
}

/**
 * 
 * Builds links for nodes/comments
 * @param $type node or comment
 * @param unknown_type $item
 * @param boolean $teaser if teaser or not
 * @return link array
 */
function hidden_links($type, $item = NULL, $teaser = FALSE) {
  $links = array();
  if (!$item) {
    return $links;
  }

  if ($type == 'comment') {
    $target = $item->cid;
    $iscomment = TRUE;
  }
  elseif ($type == 'node') {
    $target = $item->nid;
    $iscomment = FALSE;
  }
  else {
    return $links;
  }

  if (!($hidden=hidden_hidden_get($type, $target))) {
    if (user_access('mark as hidden')) {
      $links['hidden-hide'] = array(
        'title' => t('hide'),
        'href' => "hidden/$type/$target/hide",
        'query' => drupal_get_destination(),
        'fragment' => ($iscomment) ? "comment-$target" : '',
      );
    }
  }
  else {
    if (user_access('mark as hidden')) {
      $links['hidden-unhide'] = array(
        'title' => t('unhide'),
        'href' => "hidden/$type/$target/unhide",
      );
    }
  }

  return $links;
}
/**
 * Formats reason for hiding in node and comment display.
 *
 * This is shown in all views of hidden nodes and comments, it's called from the
 * nodeapi('view') and comment('view') hooks.
 *
 * @param $title
 *   string plain text standard reason for hiding.
 * @param $public
 *   string html public note about hiding.
 * @param $description
 *   string html description of standard reason for hiding (optional).
 * @param $private
 *   string html private note about hiding (optional).
 * @param $content
 *   string html formated node/comment (optional).
 * @param $view
 *   bool FALSE if short display without additional info and content (for teaser view) (optional).
 * @return
 *   string formatted output
 * @ingroup themeable
 */
function theme_hidden_view_reason($arguments = array()) {
  $title = check_plain($arguments['hidden']->title);
  $public = isset($arguments['hidden']->publicnote) ? check_plain($arguments['hidden']->publicnote) : "";
  $description = isset($arguments['hidden']->description) ? check_plain($arguments['hidden']->description) : "";
  $private = isset($arguments['hidden']->privatenote) ? check_plain($arguments['hidden']->privatenote) : "";
  $content = $arguments['content'];
 
  $output = '<div class="hidden"><div class="reason">';
  $output .= t('<p>Hidden content:</p>');
  if ($title) {
    $output .= '<p class="reasontitle">'. $title .'</p>';
    if ($arguments['full']) {
      $output .= '<p class="reasondesc">'.$description.'</p>';
    }
  }
  $output .= '<p class="hiddennote">'.$public.'</p>';
  if (user_access('mark as hidden') && $arguments['full']) {
    $output .= '<p class="hiddenpriv">'.$private.'</p>';
  }
  $output .= '</div>';
  if ($arguments['full']) {
    $output .= $content;
  }
  $output .= '</div>';
  return $output;
}

/**
 * Admin node_operations() callback.
 */
function hidden_operations_hide($nids) {
  global $user;

  $hide = array();
  $error = array();
  foreach ($nids as $nid) {
    if (! hidden_hidden_check('node', $nid)) {
      $nid = (int)$nid;
      if (hidden_hidden_hide('node', $nid, $user->uid, 1)) {
        $hide[] = $nid;
      }
      else {
        $error[] = $nid;
      }
    }
  }
  if ($n = count($error)) {
    $msg = format_plural($n, 'Error hiding node.', 'Error hiding nodes.');
    watchdog('hidden', t($msg), array(), WATCHDOG_ERROR);
    drupal_set_message($msg, 'error');
  }
  if ($n = count($hide)) {
    $msg = format_plural($n, 'Hidden node.', 'Hidden nodes.');
    watchdog('hidden', t($msg), array(), WATCHDOG_INFO);
    drupal_set_message($msg);
  }

  drupal_goto('admin/content/node');
}

/**
 * Admin node_operations() callback.
 */
function hidden_operations_unhide($nids) {
  $unhide = array();
  $error = array();
  foreach ($nids as $nid) {
    $nid = (int)$nid;
    if (hidden_hidden_check('node', $nid)) {
      if (_hidden_hidden_unhide('node', $nid)) {
        $unhide[] = $nid;
      }
      else {
        $error[] = $nid;
      }
    }
  }
  if ($n = count($error)) {
    $msg = format_plural($n, 'Error unhiding node.', 'Error unhiding nodes.');
    watchdog('hidden', t($msg), array(), WATCHDOG_ERROR);
    drual_set_message($msg, 'error');
  }
  if ($n = count($unhide)) {
    $msg = format_plural($n, 'Unhidden node.', 'Unhidden nodes.');
    watchdog('hidden', t($msg), array(), WATCHDOG_INFO);
    drupal_set_message($msg);
  }
}

/**
 * Checks if item is hidden.
 *
 * @param $type
 *   string content 'comment' or 'node'.
 * @param $id
 *   int cid or nid.
 * @return
 *   bool TRUE hidden FALSE not/fail.
 */
function hidden_hidden_check($type, $id) {
  /*static $hidden, $ltype, $lid, $count;
  if ($type == $ltype && $id == $lid) {
    return $hidden;
  }
  else {
    $ltype = $type;
    $lid = $id;
    $hidden = NULL;
  }*/

  if ($type == 'node' || $type == 'comment') {  
    $table = ($type == 'node') ? 'node' : 'comment';
    $col = ($type == 'node') ? 'nid' : 'cid';
    $query = "SELECT COUNT(*) FROM {hidden_$table} WHERE $col = :d";

    $result = db_query($query, array(":d" => $id));
    if ($result->rowCount() != 0) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Gets a hidden item.
 *
 * @param $type
 *   string content type 'comment' or 'node'
 * @param $id
 *   int cid or nid
 * @return
 *   object -> hid, rid, title, description, publicnote, privatenote, or FALSE not hidden
 */
function hidden_hidden_get($type, $id) {
  if ($type == 'node') {
    $query = 'SELECT h.hid, h.nid, h.rid, h.uid, h.publicnote, h.privatenote, h.delay,'.
            ' r.title, r.description'.
            " FROM {hidden_node} AS h".
            ' LEFT JOIN {hidden_reasons} AS r ON h.rid=r.rid'.
            " WHERE nid=:id";
  }
  elseif ($type == 'comment') {
    $query = 'SELECT h.hid, h.cid, h.rid, h.uid, h.publicnote, h.privatenote, h.delay,'.
            ' r.title, r.description'.
            " FROM {hidden_comment} AS h".
            ' LEFT JOIN {hidden_reasons} AS r ON h.rid=r.rid'.
            " WHERE cid=:id";
  }
  else {
    return FALSE;
  }

  if ($hidden = db_query($query, array(":id"=>$id))->fetchObject()) {
      $hidden->type = $type;
      $hidden->id = $id;
      return $hidden;
  }
  return FALSE;
}

/**
 * Removes hidden items from table.
 *
 * @param $type
 *   string content type to remove 'comment', 'node' or 'both'.
 * @param $id
 *   int cid (comment only) or nid (node only or both).
 * @return
 *   FALSE on fail.
 */
function hidden_hidden_delete($type, $id) {

  if ($type == 'both' || $type == 'node') {
    //$query = 'DELETE FROM {hidden_node} WHERE nid=%d';
    //$dq = db_delete('hidden_node')->condition('nid', $id);
    // error catching
    if (db_delete('hidden_node')->condition('nid', $id)->execute()<=0) {
      return FALSE;
    }
  }

  if ($type == 'comment') {
    //$query = 'DELETE FROM {hidden_comment} WHERE cid=%d';
    $dq = db_delete('hidden_comment')->condition('cid', $id);
  }
  elseif ($type == 'both') {
    //$query = 'DELETE FROM {hidden_comment} WHERE nid=%d';
    $dq = db_delete('hidden_comment')->condition('nid', $id);
  }
  elseif ($type == 'node') {
    return TRUE;
  }
  if ($dq->execute>0) {
    return TRUE;
  }
  return FALSE;

  //return db_query($query, $id);
}

/**
 * Performs db queries required to hide an item.
 *
 * Inserts into hidden table.
 * Also if needed marks as seen in hidden_reported, unpublishes, stops comments on nodes.
 *
 * @param $nid
 *   int nid.
 * @param $cid
 *   int if hiding a comment, if node pass '0'.
 * @param $rid
 *   int id of reason for hiding (optional).
 * @param $public
 *   string html of public note (optional).
 * @param $private
 *   string html of private note (optional).
 * @param $filter
 *   int if called by filtering time to delay posting till, 0 for being done
 * @return
 *   FALSE on fail. hid on success.
 */
function hidden_hidden_hide($type, $id, $uid, $rid=0, $public='', $private='') {
  
  if ($hidden = hidden_hidden_get($type, $id)) {
      drupal_set_message(t('The %type is already hidden!', array('%type' => $type)), 'error');
      return FALSE;
  }
  else {
    // Insert into hidden table
    if ($type == 'node') {
      $hidresult = db_insert('hidden_node')
        ->fields(array(
          'nid' => $id,
          'uid' => $uid,
          'created' => REQUEST_TIME,
          'rid' => $rid,
          'publicnote' => $public,
          'privatenote' => $private,
          //'delay' => $filter,
        ))->execute();
    }
    else {
      $hidresult = db_insert('hidden_comment')
        ->fields(array(
          'cid' => $id,
          'uid' => $uid,
          'created' => REQUEST_TIME,
          'rid' => $rid,
          'publicnote' => $public,
          'privatenote' => $private,
          //'delay' => $filter,
        ))->execute();
    }
  }
  if (! $hidresult) {
    watchdog('hidden', t("hidden_hidden_hide() failed to insert into hidden_$type."), array(), WATCHDOG_ERROR);
    return FALSE;
  }

  if (! hidden_hidden_hide_unpublish($type, $id)) {
    watchdog('hidden', t("Error unpublishing $type on hiding."), array(), WATCHDOG_ERROR);
    return FALSE;
  }

  return TRUE;
}

/**
 * Unpublishes a node or comment
 *
 * Updates comments to status COMMENT_NOT_PUBLISHED
 * and nodes to status 0, and stops further comments
 *
 * @todo consider loading objects, manipulating (with actions), and saving
 *
 * @param $type
 *   string 'node' or 'comment'
 * @param $id
 *   int nid or cid
 * @return
 *   TRUE on success
 */
function hidden_hidden_hide_unpublish($type, $id) {
  if ($type == 'comment') {
    //$updated = db_update('comment')->fields(array('status'=>COMMENT_NOT_PUBLISHED))->condition('cid', $id, '=')->execute();
    $comment = comment_load($id, TRUE);
    //comment_unpublish_action($comment);
    $comment->status = COMMENT_NOT_PUBLISHED;
    comment_save($comment);
    return TRUE;
  }
  elseif ($type == 'node') {
    //$updated = db_update('node')->fields(array('status'=>NODE_NOT_PUBLISHED, 'comment'=>1))->condition('nid', $id, '=')->execute();
    $node = node_load($id, NULL, TRUE);
    //node_unpublish_action($node);
    $node->published = NODE_NOT_PUBLISHED;
    $node->comment = COMMENT_NODE_CLOSED;
    //$node = node_submit($node);
    node_save($node);
    return TRUE;
  }
  return FALSE;
}

/**
 * Performs db queries required to unhide an item.
 *
 * @param $type
 *   string type 'node' or 'comment'.
 * @param $id
 *   int nid or cid.
 * @return
 *   bool TRUE on success.
 */
function hidden_hidden_unhide($type, $id) {
  if ($type == 'comment') {
    $dq = db_delete('hidden_comment')->condition('cid', $id);
    //$query = 'DELETE FROM {hidden_comment} WHERE cid = :d';
  }
  elseif ($type == 'node') {
    //$query = 'DELETE FROM {hidden_node} WHERE nid = :d';
    $dq = db_delete('hidden_node')->condition('nid', $id);
  }
  else {
    return FALSE;
  }

  //$result = db_query($query, array(":d" => $id));
  $result = $dq->execute();
  if (!$result) {
    drupal_set_message(t('Database error occurred when deleting item from hidden table.'), 'error');
    return FALSE;
  }

  return hidden_hidden_publish($type, $id);

  /*if ($result) {
    return TRUE;
  }
  else {
    return FALSE;
  }*/
}

/**
 * Opposite actions to _hidden_hidden_unpublish().
 *
 * @todo set comment status to correct for type, or back to previous settings (need storing).
 */
function hidden_hidden_publish($type, $id) {
  if ($type == 'comment') {
    $comment = comment_load($id);
    comment_publish_action($comment);
    comment_save($comment);
    return TRUE;
  }
  else {
    // set comments to read/write
    $node = node_load($id);
    node_publish_action($node);
    $node->comment = 2;
    node_save($node);
    return TRUE;
  }

  return FALSE;
}


/********************************************************************
 * Hidden Reasons
 */

/**
 * Checks if a rid is a reason.
 *
 * @param $rid
 *   int reason id.
 * @return
 *   bool TRUE on reason id being valid.
 */
function hidden_reason_check($rid) {
  $sql = 'SELECT COUNT(*) FROM {hidden_reasons} WHERE rid=:d';
  if (db_query($sql, array(":d" => $rid))->fetchField() != 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Returns array of all hidden reasons.
 *
 * @param $only_enabled
 *   bool return only enabled reasons, default TRUE.
 * @param $cache
 *   bool use cached results default TRUE.
 * @return
 *   object array of reason objects.
 */
function hidden_reason_get_all($only_enabled = TRUE) {
  return hidden_reason_get(-1, $only_enabled);
}

/**
 * Internal function to handle storing and returning reasons.
 *
 * Cached as used as a non-join table used by views.
 *
 * @param $rid
 *   int reason id (or -1 for all reasons).
 * @param $only_enabled
 *   bool only return enabled reasons.
 * @param $cache
 *   bool use cache.
 * @return
 *   array of reason object, an inidividual hidden object, FALSE
 */
function hidden_reason_get($rid, $only_enabled) {
  $reasons = array();
  $query = 'SELECT * FROM {hidden_reasons}';
  if ($only_enabled) {
    $query .= ' WHERE enabled=1';
  }
  $result = db_query($query);
  foreach($result as $reason) {
    $reasons[$reason->rid] = $reason;
  }

  if ($rid > 0) {
    return isset($reasons[$rid]) ? $reasons[$rid] : FALSE;
  }
  else {
    return $reasons;
  }
}

// @todo all of these can go into hidden_reason_write() :-

/**
 * Update hidden reason.
 *
 * @param $title
 *   string plain text(255) title for hidden_reason.
 * @param $description
 *   string html text describing reason
 * @param $enabled
 *   bool TRUE for an enabled reason
 * @param $rid
 *   rid - reason id
 * @return
 *   FALSE fail, SAVED_* const for type of saved
 */
function hidden_reason_update($title, $description, $enabled = FALSE, $rid) {
  if (hidden_reason_check($rid)) {
    if (!($rid = db_update('hidden_reasons')->fields(array('title'=>$title, 'description'=>$description, 'enabled'=>$enabled ? 1 : 0))->condition('rid', $rid, '=')->execute())) {
      watchdog('hidden', "Updating hidden_reasons failed %rid.", array('%rid'=>$rid));
      return FALSE;
    }
    return $rid;
  }
  return FALSE;
}

/**
 * Create hidden reason
 *
 * @param $title
 *   string plain text(255) title for reason.
 * @param $description
 *   string html text description of reason.
 * @param $enabled
 *   bool TRUE for an enabled reason.
 */
function hidden_reason_create($title, $description, $enabled = TRUE) {
  if (!($rid = db_insert('hidden_reasons')->fields(array('title'=>$title, 'description'=>$description, 'enabled'=>$enabled))->execute())) {
    watchdog('hidden', "Inserting into hidden_reasons failed");
    return FALSE;
  }
  return $rid;
}

/**
 * Enable a reason.
 *
 * @param $rid
 *   rid
 * @return
 *   bool TRUE on success
 */
function hidden_reason_enable($rid) {
  if (!($rid = db_update('hidden_reasons')->fields(array('enabled'=>1))->condition('rid', $rid, '=')->execute())) {
    watchdog('hidden', t('enabling of hidden reason failed'), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  return TRUE;
}

/**
 * Disable a reason
 *
 * @param $rid
 *   rid
 * @return 
 *   bool TRUE on success
 */
function hidden_reason_disable($rid) {
  if (!($rid = db_update('hidden_reasons')->fields(array('enabled'=>0))->condition('rid', $rid, '=')->execute())) {
    watchdog('hidden', t('disablding of hidden reason failed'), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  return TRUE;
}