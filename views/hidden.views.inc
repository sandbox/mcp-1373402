<?php

function hidden_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'hidden'),
    ),
    'handlers' => array(
      'hidden_handler_filter_boolean_operator_int' => array(
        'parent' => 'views_handler_filter_boolean_operator',
        'file' => 'hidden_handler_filter_boolean_operator_int.inc',
      ),
    ),
  );
}

function hidden_views_data() {
  $data = array(
    /* the hidden_node table */
    'hidden_node' => array(
      'table' => array(
        'group' => t('Hidden'),
        'join' => array(
          'node' => array(
            'left_field' => 'nid',
            'field' => 'nid'
          ),
        ),
      ),
      'nid' => array(
        'title' => t('The Node ID'),
        'help' => t('The nid of the hidden node'),
        'relationship' => array(
          'base' => 'node',
          'field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Hidden node'),
        ),
      ),
      'created' => array(
        'title' => t('Hide Timestamp'),
        'help' => t('When the node was hidden'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),
      'publicnote' => array(
        'title' => t('Public Note'),
        'help' => t("The public note for the hiding of a node"),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'privatenote' => array(
        'title' => t('Private Note'),
        'help' => t("The private note for the hiding of a node"),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'uid' => array(
        'title' => t('User ID'),
        'help' => t('The User who hid the node'),
        'field' => array(
          'handler' => 'views_handler_field_user',
        ),
      ),
      /* this is a hack to filter hidden/not hidden nodes */
      'hidden' => array(
        'real field' => 'rid',
        'title' => t('Hidden'),
        'help' => t('If the node is hidden'),
        'filter' => array(
          'handler' => 'hidden_handler_filter_boolean_operator_int',
          'label' => t('Hidden'),
          'type' => 'yes-no',
        ),
      ),
    ),
    /* the hidden_reasons table */
    'hidden_reasons' => array(
      'table' => array(
        'group' => t('Hidden'),
        'title' => t('Reasons'),
        'join' => array(
          'node' => array(
            'left_table' => 'hidden_node',
            'left_field' => 'rid',
            'field' => 'rid',
          ),
          'comments' => array(
            'left_table' => 'hidden_comment',
            'left_field' => 'rid',
            'field' => 'rid',
          ),
        ),
      ),
      'rid' => array(
        'title' => t('Reason ID'),
        'help' => t('The ID of the hidden reason'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
      ),
      'title' => array(
        'title' => t('Reason'),
        'help' => t('The title of the reason'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'description' => array(
        'title' => t('Description'),
        'help' => t('The description of the reason for hiding'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
    ),
    /* the hidden_comment table */
    'hidden_comment' => array(
      'table' => array(
        'group' => t('Hidden'),
        'title' => t('Hidden comments'),
        'join' => array(
          'comments' => array(
            'left_field' => 'cid',
            'field' => 'cid',
          ),
        ),
      ),
      'cid' => array(
        'title' => t('The comment ID'),
        'help' => t('The cid of the hidden comment'),
        'relationship' => array(
          'base' => 'comments',
          'field' => 'cid',
          'handler' => 'views_handler_relationship',
          'label' => t('Hidden comment'),
        ),
      ),
      'created' => array(
        'title' => t('Hide timestamp'),
        'help' => t('When the comment was hidden'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
      ),
      'publicnote' => array(
        'title' => t('Public Note'),
        'help' => t("The public note for the hiding the comment"),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'privatenote' => array(
        'title' => t('Private Note'),
        'help' => t("The private note for the hiding the comment"),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'uid' => array(
        'title' => t('User ID'),
        'help' => t('The User who hid the comment'),
        'field' => array(
          'handler' => 'views_handler_field_user',
        ),
      ),
      /* this is a hack to filter hidden/not hidden nodes */
      'hidden' => array(
        'real field' => 'rid',
        'title' => t('Hidden'),
        'help' => t('If the comment is hidden'),
        'filter' => array(
          'handler' => 'hidden_handler_filter_boolean_operator_int',
          'label' => t('Hidden'),
          'type' => 'yes-no',
        ),
      ),
    ),
  );
  return $data;
}

