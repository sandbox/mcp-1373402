<?php
class hidden_handler_filter_boolean_operator_int extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    $where = "$this->table_alias.$this->real_field ";

    if (empty($this->value)) {
      $where .= "IS NULL";
    } else {
      $where .= "IS NOT NULL";
    }
    $this->query->add_where($this->options['group'], $where);
  }
}
